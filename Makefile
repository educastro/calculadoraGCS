#-------------------------------------------------------------------------------
# Nome do projeto
#-------------------------------------------------------------------------------
NAME=calculadoraGCS


SRC = ${wildcard *.c}
OBJ = ${notdir ${SRC:.c=.o}}
INC = -I.

#-------------------------------------------------------------------------------
# Compilador, flags e bibliotecas
#-------------------------------------------------------------------------------
CC=gcc
CFLAGS= -Wall -Wextra -pedantic -ansi -g -std=c99
LIB=

TARGET=$(NAME)

.PHONY: clean all

all:
	@echo Compiling...
	$(MAKE) $(TARGET)

%.o: %.c
	@echo Building $@
	$(CC) -c $^ -o $@ $(CFLAGS) $(INC)

$(TARGET): $(OBJ)
	@echo Linking $@
	$(CC) -o $@ $(OBJ) $(LIB)

clean:
	@echo Cleaning...
	@rm -rvf *~ *.o $(TARGET)

.PRECIOUS: %.o
