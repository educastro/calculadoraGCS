#include <stdio.h>
#include <stdlib.h>

/*
 * Sum is a mathematical operation with two numbers
 */
double sum(double value1, double value2) {
   double result = value1 + value2;
   return result;
}

//time operation
double multiply(double value1, double value2) {
   double result = value1 * value2;
   return result;
}

/*
* Substraction is a mathematical operation with two numbers that represets
* its substraction. Example: 1 - 1 = 0
*/
double subtraction(double value1, double value2)
{
	double result = value1 - value2;
	return result;
}

double division(double number1,double number2)
{
	double result = number1/number2;
	return result;
}
