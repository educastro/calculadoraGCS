// Main

#include <stdio.h>
#include <stdlib.h>
#include "calculadora.h"

int main(int argc, char* argv[])
{
  double operando_1, operando_2;
  char operacao;

  operando_1 = atof(argv[1]);
  operacao = argv[2][0];
  operando_2 = atof(argv[3]);

  switch(operacao) {
    case '+':
      fprintf(stderr, "Resultado: %lf\t", sum(operando_1, operando_2));
      break;
    case '-':
      fprintf(stderr, "Resultado: %lf\t", subtraction(operando_1, operando_2));
      break;
    case 'x':
      fprintf(stderr, "Resultado: %lf\t", times(operando_1, operando_2));
      break;
    case '/':
      fprintf(stderr, "Resultado: %lf\t", division(operando_1, operando_2));
      break;            
    default:
      fprintf(stderr, "Nenhum operando válido foi identificado.");
      break;
  }

  fprintf(stderr,"\nOperando 1: %lf\t", operando_1);
  fprintf(stderr,"Operando 2: %lf\t", operando_2);
  fprintf(stderr,"Operador: %c\n", operacao);
  fprintf(stderr, "%lf %c %lf\n", operando_1, operacao, operando_2);

  return 0;
}
